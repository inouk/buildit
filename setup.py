import setuptools
from setuptools import setup, find_packages


setup(
    name='inouk.buildit',
    version='0.0.2.dev1',
    packages=[
        'inouk', 
        'inouk.buildit', 
    ],
    url="https://gitlab.com/inouk/buildit",
    entry_points={
        'console_scripts': ['ikb = inouk.buildit.main:program.run']
    },
    install_requires=[
          'pyyaml>=5.4.1',
          'invoke>=1.7.3',
          'jsonc-parser>=1.1.5',
      ],    
)
