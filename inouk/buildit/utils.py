import re


BUILDIT_PROPERTY_REGEX = re.compile(r'\$\{(?P<part>[0-9a-zA-Z_]+)(\:(?P<property>[0-9a-zA-Z_]+))?\}')


def parse_value(c, val):
    """ parse a value of form ${part:property} """
    #print("DEBUG xxxxxxxxxxxxxxxxxxxxxxxxx parse_value(%s)", val)
    parsed_val = val
    if isinstance(val, str):
        for m in BUILDIT_PROPERTY_REGEX.finditer(val):
            mg = m.groupdict()
            part_name = mg.get('part')
            prop_name = mg.get('property')
            part = c.config.buildit[part_name]
            #print("DEBUG  xxxxxxxxxxxxxxxxxxxxxxxxx part_name=%s" % part_name)
            #print("DEBUG  xxxxxxxxxxxxxxxxxxxxxxxxx prop_name=%s" % prop_name)
            if prop_name:
                if prop_name in part:
                    prop_value = part.get(prop_name)
                    parsed_val = parsed_val.replace(m.group(), prop_value)
                else:
                    return None
            else: 
                return part
    return parsed_val
