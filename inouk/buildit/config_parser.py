import copy
import json
import os
import types
import yaml
from os.path import join, splitext, expanduser
import logging

from jsonc_parser.parser import JsoncParser

_logger = logging.getLogger('inouk.buildit.config_parser')


class Config:

    def __init__(self, config_file_path:str=None, command_prefix='__$'):
        """
        :param config_file_name: Option buildit.json? to use. If none will try to locate it
        """
        self._cmd_prefix = command_prefix
        self._file_suffixes = ['yaml', 'json', 'jsonc']
        self._output_file_suffixes = ['yaml', 'json']
        self.config_files_dict = []
        self.config_files_path = []
        self._config = {}

        if config_file_path:
            if os.path.isfile(config_file_path):
                _file_path = os.path.expanduser(confile_file_path)
            else:
                _msg = "Unable to open config file: %s (cwd=%s)" % (config_file_path, os.getcwd())
                _logger.error(_msg)
                raise Exception(_msg)
        else:
            _file_path = self._locate_config_file()

        if not _file_path:
            self.base_config_file_path = None
            self.base_config_directory = None
            self.base_config_file_name = None
            print("ikb: No config file found (cwd=%s)." % os.getcwd())
        else:
            print("ikb: Using config file '%s' (cwd=%s)." % (_file_path, os.getcwd()))
            self.base_config_file_path = _file_path
            self.base_config_directory, self.base_config_file_name = os.path.split(_file_path)


    def _locate_config_file(self):
        """ Look for buildit.json in several locations:
            -- buildit.jsonc or buildit.json in cwd
        """
        _cwd = os.getcwd()
        if os.path.isfile("buildit.json"):
            _file_path = os.path.abspath('buildit.json')

        elif os.path.isfile("buildit.jsonc"):
            _file_path = os.path.abspath('buildit.jsonc')

        elif os.path.isfile(".ikb/buildit.json"):
            _file_path = os.path.abspath('.ikb/buildit.json')

        elif os.path.isfile(".ikb/buildit.jsonc"):
            _file_path = os.path.abspath('.ikb/buildit.jsonc')

        else:
            _logger.debug("Unable to locate config file.")
            return None
        return _file_path

    def _load_yaml(self, path):
        with open(path) as fd:
            data = yaml.load(fd)
        return data

    def _load_yml(self, path):
        return self._load_yaml(path)

    def _load_jsonc(self, path):
        data = JsoncParser.parse_file(path)
        return data

    def _load_json(self, path):
        return self._load_jsonc(path) 

    def _get_file_loader(self, file_path):
        type_ = splitext(file_path)[1].lstrip(".")
        try:
            loader = getattr(self, "_load_{}".format(type_))
        except AttributeError:
            msg = "Config files of type {!r} (from file {!r}) are not supported! Please use one of: {!r}"  # noqa
            raise Exception(
                msg.format(type_, file_path, self._file_suffixes)
            )
        return loader

    def _write_yaml(self, file_path):
        with open(file_path, mode='w') as fd:
            f_content = yaml.dump(self._config)
            r = fd.write(f_content)
        return r

    def _write_yml(self, file_path):
        return self._write_yaml(file_path)

    def _write_json(self, file_path):
        with open(file_path, mode='w') as fd:
            f_content = json.dumps(self._config, indent=4)
            r = fd.write(f_content)
        return r
        
    def _get_file_writer(self, file_path):
        _file_type = splitext(file_path)[1].lstrip(".")
        try:
            _method = getattr(self, "_write_{}".format(_file_type))
        except AttributeError:
            raise Exception(
                f"Config files of type {_file_type} (from file {file_path}) are not supported! Please use one of: {self._output_file_suffixes}"  # noqa
            )
        return _method

    def write(self, file_path):
        """ Generate merge config in file identified by file_path.
        When file_path is relative, file is generated in base_config_directory
        :returns: absolute path of written file.
        """
        if not file_path:
            raise Exception("Missing required file_path")
        if not self._config:
            return None
        if not os.path.isabs(file_path):
            file_path = os.path.join(self.base_config_directory, file_path)
        writer =  self._get_file_writer(file_path)
        _r  = writer(file_path)
        return file_path

    def _load_file(self, file_path):
        if not os.path.isabs(file_path):
            file_path = os.path.join(self.base_config_directory, file_path)
        try:
            # Store data, the path it was found at, and fact that it was found
            loader =  self._get_file_loader(file_path)
            _d  = loader(file_path)
            return _d, file_path
        # Typically means 'no such file', so just note & skip past.
        except IOError as e:
            if e.errno == 2:
                err = f"Didn't found any '{file_path}'."
                _logger.error(err)
            else:
                raise

    def _load_config_files_chain(self, config_file_path:str):
        """ recursively load confif files and storing them in """
        _extends_cmd = f"{self._cmd_prefix}extends"
        _base_path = config_file_path
        if not os.path.isabs(config_file_path):
            config_file_path = os.path.join(self.base_config_directory, config_file_path)
            if not os.path.isfile(config_file_path):
                config_file_path = os.path.join(self.base_config_directory, ".ikb/%s" % _base_path)

        if not os.path.isfile(config_file_path):
            _logger.error("Config file not found:'%s'.", config_file_path)
            return False
        _file_dict, _file_name = self._load_file(config_file_path)
        
        overloaded_file = _file_dict.get(_extends_cmd)  # type: ignore
        if overloaded_file:
            self._load_config_files_chain(overloaded_file)
            del _file_dict[_extends_cmd]
        self.config_files_dict.append(_file_dict)
        self.config_files_path.append(_file_name)
        return True

    def _copy_dict(self, source_dict):
        """
        Return a new instance of ``source_dict``.
        """
        return self._merge_dicts({}, source_dict)

    def _merge_error(self, origin, merged):
        return Exception(
            f"Can't cleanly merge {origin} with {merged}"
        )
            
    def _merge_lists(self, base, key, updates, command=None):
        """
        Recursively merge list ``updates`` into list ``base`` (mutating ``base``.)

        * Values which are themselves dicts will be recursed into.
        * Values which are a list in one input and *not* a list in the other input
        (e.g. if our inputs were ``['foo', 5]`` and ``{'foo': {'bar': 5}}``) are
        irreconciliable and will generate an exception.
        * Non-dict and non-list leaf values are run through `copy.copy` to avoid 
        state bleed.
        * We accept ``base`` and ``key`` parameters to retreive a reference to the 
        base list since we need to mutate it.

        :param command: supported commands are: 'let' (default), 'merge' and 'delete'
        .. note::
            This is effectively a lightweight `copy.deepcopy` which offers
            protection from mismatched types (dict vs non-dict) and avoids some
            core deepcopy problems (such as how it explodes on certain object
            types).
            Only dicts elem are recursively merged.
        :returns:
            The value of ``base[key]``, which is mostly useful for wrapper functions
            like `copy_dict`.

        .. versionadded:: 1.0
        """
        if command is None:
            command = 'let'
        
        if command == 'let':
            base[key] = copy.copy(updates)
            return base[key]

        for update_elem in updates:
            if command=='merge':
                # Do we have an updatable dict; a dict with a 'name' kay
                if isinstance(update_elem, dict) and update_elem.get('name'):
                    inner_key = update_elem['name']
                    for base_elem in base[key]:
                        if isinstance(base_elem, dict) and base_elem.get('name')==inner_key:
                            self._merge_dicts(base_elem, update_elem)
                elif update_elem not in base[key]:
                    base[key].append(update_elem)

            elif command=='delete':
                if update_elem in base[key]:
                    base[key].remove(update_elem)

            elif command:
                raise Exception("Command: '%s' is not Implemented!!" % command)

        return base[key]

    def _merge_dicts(self, base_dict:dict, updates_dict:dict, name:str=''):
        if name:
            _logger.debug(f"Merging '{name}' into self._config")
        """
        Recursively merge dict ``updates`` into dict ``base`` (mutating ``base``.)

        * Values which are themselves dicts will be recursed into.
        * Values which are a dict in one input and *not* a dict in the other input
        (e.g. if our inputs were ``{'foo': 5}`` and ``{'foo': {'bar': 5}}``) are
        irreconciliable and will generate an exception.
        * Non-dict leaf values are run through `copy.copy` to avoid state bleed.

        .. note::
            This is effectively a lightweight `copy.deepcopy` which offers
            protection from mismatched types (dict vs non-dict) and avoids some
            core deepcopy problems (such as how it explodes on certain object
            types).

        :returns:
            The value of ``base``, which is mostly useful for wrapper functions
            like `copy_dict`.

        .. versionadded:: 1.0
        """
        if not updates_dict: updates_dict = {}
        for key, value in updates_dict.items():
            if self._cmd_prefix in key:
                key, cmd = key.split(self._cmd_prefix)
            else:
                cmd = None

            if key in base_dict:
                if isinstance(value, dict):
                    if isinstance(base_dict[key], dict):
                        self._merge_dicts(base_dict[key], value)
                    else:
                        raise self._merge_error(base[key], value)
                elif isinstance(value, list):
                    if isinstance(base_dict[key], list):
                            self._merge_lists(base_dict, key, value, command=cmd)
                    else:
                        raise self._merge_error(base_dict[key], value)
                else:
                    if isinstance(base_dict[key], (dict, list,)):
                        raise self._merge_error(base_dict[key], value)
                    # Fileno-bearing objects are probably 'real' files which do not
                    # copy well & must be passed by reference. Meh.
                    elif hasattr(value, "fileno"):
                        base_dict[key] = value
                    elif isinstance(base_dict, dict) and isinstance(value, str) and value==f"{self._cmd_prefix}delete":
                        del base_dict[key]
                    else:
                        base_dict[key] = copy.copy(value)
            # New values get set anew
            else:
                # Dict values get reconstructed to avoid being references to the
                # updates dict, which can lead to nasty state-bleed bugs otherwise
                if isinstance(value, dict):
                    base_dict[key] = self._copy_dict(value)
                # Fileno-bearing objects are probably 'real' files which do not
                # copy well & must be passed by reference. Meh.
                elif hasattr(value, "fileno"):
                    base_dict[key] = value
                # Non-dict values just get set straight
                else:
                    base_dict[key] = copy.copy(value)
        return base_dict

    def parse(self):
        """
        :returns: parsed config as dict or None
        """
        if not self.base_config_file_name:
            return None
        _r = self._load_config_files_chain(self.base_config_file_path)
        if not _r:
            return None
        if self.config_files_dict:
            _logger.debug(f"Setting '{self.config_files_path[0]}' as base config dict")
            self._config = self.config_files_dict[0]

        for idx, config_dict in enumerate(self.config_files_dict[1:]):
            self._merge_dicts(self._config, config_dict, name=self.config_files_path[idx+1])
        return self._config


if __name__ == '__main__':

    LOG_FORMAT = "%(name)s.%(module)s.%(funcName)s: %(message)s"
    logging.basicConfig(level=logging.DEBUG, format=LOG_FORMAT)

    i = Config( "/home/cmorisse/appserver-mpy13crd/buildit.jsonc")
    _r = i.parse()
    i.write('final.buildit.json')
    i.write('final.buildit.yaml')

# Portions Copyright (c) 2020 Jeff Forcier.
# All rights reserved.
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright notice,
#       this list of conditions and the following disclaimer in the documentation
#       and/or other materials provided with the distribution.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# Footer
