import importlib
import json
import logging
import os
import logging
import sys

from invoke import task, Collection
from invoke.config import DataProxy

_logger = logging.getLogger(__name__)

plugin_module_dict = {}


ROOT_DIRECTORY = os.getcwd()
RUNNING_ENV_ROOT_DIR = None

@task
def setup_ikb_plugins(c):
    """ Install all inouk.buildit part's plugins. 
    Plugins are defined using the "plugin" attribute (which is an eggspec) of each part.
    """
    print("TODO:")
    print(" - scan buildit.json for all plugins.")
    print("   pip install each plugin using according to eggspec.")
    # TODO: Try to import each plugin, pip install it it it fails.
    pass

def add_ikb_plugins_to_python_path(c):
    _logger.debug("Adding ikb plugins directory to sys.path")
    #
    # Setting paths for plugins
    ROOT_DIRECTORY = c.config.buildit.root_directory
    plugin_sys_path = "%s/.ikb/plugins" % RUNNING_ENV_ROOT_DIR
    if plugin_sys_path not in sys.path and os.path.isdir(plugin_sys_path):
        sys.path.append(plugin_sys_path)

    plugin_sys_path = "%s/buildit-plugins" % RUNNING_ENV_ROOT_DIR
    if plugin_sys_path not in sys.path and os.path.isdir(plugin_sys_path):
        sys.path.append(plugin_sys_path)



@task
def init(c):
    _logger.critical("CWD: %s", ROOT_DIRECTORY)
    # TODO: Check for a buildit.json file in CWD and abort if not found
    #opt = c.config['buildit']  # => DataProxy
    if hasattr(c.config, 'buildit'):
        opt = c.config.buildit  # => DataProxy => we need this to be able to update
        #opt = c.config.get('buildit')  # => dict

        # We inject a 'root_directory' part so that it can be used in Tasks
        c.config.buildit['root_directory'] = ROOT_DIRECTORY
        print("Building parts: %s" % ','.join(opt.keys()))

        # We inject running env root folder
        RUNNING_ENV_ROOT = os.environ.get(
            'IKB_RUNNING_ENV_ROOT_PATH', 
            c.config.buildit.get(
                'running-env-root-path',
                ROOT_DIRECTORY
            )
        )
        if os.path.isabs(RUNNING_ENV_ROOT):
            c.config.buildit['running_env_root_dir'] = RUNNING_ENV_ROOT
        else:
            c.config.buildit['running_env_root_dir'] = os.path.abspath(os.path.join(ROOT_DIRECTORY, RUNNING_ENV_ROOT))
        global RUNNING_ENV_ROOT_DIR
        RUNNING_ENV_ROOT_DIR = c.config.buildit['running_env_root_dir']

        global plugin_module_dict

        # Init phase
        ns = Collection() 
        for part_name in opt:
            part = opt[part_name]
            if type(part) in (dict, DataProxy,) and part.get('plugin', False):
                plugin = part.get('plugin')
                print("Initializing part '%s' with plugin:'%s'." % (part_name, plugin,))
                add_ikb_plugins_to_python_path(c)
                plugin_module = importlib.import_module(plugin)
                plugin_module_dict[part_name] = plugin_module
                plugin_collection = Collection.from_module(plugin_module)
                ns.add_collection(plugin_collection)
                plugin_module.init(c, part_name)
            else:
                _logger.debug("Skipping '%s' as it does not seem to be a plugin (no plugin attribute)" % part)
    else:
        print("RED: No buildit config found! Processing stopped")
        sys.exit(1)

def custom_task_init(c, plugin_name):
    init(c)
    parts_dict = {}
    opt = c.config.buildit
    for part_name in opt:
        part = opt[part_name]
        if type(part) in (dict, DataProxy,) and part.get('plugin', False)==plugin_name:
            parts_dict[part_name] = part
    return parts_dict



@task(init)
def prerequisites(c):
    """ Install system prerequisites. """
    opt = c.config.get('buildit')  # => dict
    # Install phase
    for part_name in opt:
        part = opt[part_name]
        if type(part) in (dict, DataProxy,) and part.get('plugin', False):
            plugin = part.get('plugin')
            plugin_module = plugin_module_dict[part_name]
            if plugin_module:
                if hasattr(plugin_module, 'prerequisites'):
                    print("Installing Prerequisites for part:'%s' with plugin:'%s'." % (part_name, plugin,))
                    plugin_module.prerequisites(c, part_name)

        else:
            _logger.debug("Skipping '%s' as it does not seem to be a plugin (no plugin attribute)" % part)



@task(init)
def install(c):
    opt = c.config.get('buildit')  # => dict
    # Install phase
    for part_name in opt:
        part = opt[part_name]
        if type(part) in (dict, DataProxy,) and part.get('plugin', False):
            plugin = part.get('plugin')
            plugin_module = plugin_module_dict[part_name]
            if plugin_module:
                if hasattr(plugin_module, 'install'):
                    print("Installing part:'%s' with plugin:'%s'." % (part_name, plugin,))
                    plugin_module.install(c, part_name)

        else:
            _logger.debug("Skipping '%s' as it does not seem to be a plugin (no plugin attribute)" % part)


@task(init)
def update(c):
    """ Reserved for future use. """
    return



@task(init)
def reset(c):
    opt = c.config.get('buildit')  # => dict
    for part_name in opt:
        part = opt[part_name]
        if type(part) in (dict, DataProxy,) and part.get('plugin', False):

            plugin_module = plugin_module_dict[part_name]
            plugin = part.get('plugin')
            if plugin_module:
                if hasattr(plugin_module, 'reset'):
                    print("Resetting part '%s' (plugin='%s')." % (part_name, plugin,))
                    plugin_module.reset(c, part_name)
        else:
            _logger.debug("Skipping '%s' as it does not seem to be a plugin (no plugin attribute)" % part)



