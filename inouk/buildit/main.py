import sys
import importlib
import os
import pkgutil
import logging
LOG_FORMAT = "%(name)s.%(module)s: %(message)s"
logging.basicConfig(level=logging.INFO, format=LOG_FORMAT)

# we load ENV VAR from /etc/muppy.env
IKB_ENVFILE = '/etc/muppy.env'
try:
    with open(IKB_ENVFILE) as f:
        for _env_line in f.read().splitlines():
            _var, _value = _env_line.split('=')
            if _var not in os.environ:
                os.environ[_var] = _value
except:
    print("Failed to read EnvFile: %s " % IKB_ENVFILE)

# This must be done before 'invoke' import below
from . import config_parser
_config_parser = config_parser.Config()
_buildit_config = _config_parser.parse()
_final_config_file_path = _config_parser.write('final.buildit.json')
if not _final_config_file_path:
    print("Aborting.") 
    sys.exit(18) 
os.environ["INVOKE_RUNTIME_CONFIG"] = _final_config_file_path

# we must figure out running-env-root-dir
if False:
    WORKING_DIRECTORY = os.getcwd()
    RUNNING_ENV_ROOT = os.environ.get(
        'IKB_RUNNING_ENV_ROOT_PATH', 
        _buildit_config.get('buildit', {}).get(
            'running-env-root-path',
            WORKING_DIRECTORY
        )
    )
    if os.path.isabs(RUNNING_ENV_ROOT):
        RUNNING_ENV_ROOT_DIR = RUNNING_ENV_ROOT
    else:
        RUNNING_ENV_ROOT_DIR = os.path.abspath(os.path.join(WORKING_DIRECTORY, RUNNING_ENV_ROOT))

    # add running env root in path
    if RUNNING_ENV_ROOT_DIR not in sys.path:
        sys.path.append(RUNNING_ENV_ROOT_DIR)

from . import tasks
from invoke import Collection, Program

_logger = logging.getLogger('inouk.buildit')
ns = Collection.from_module(tasks)

_logger.debug("Working Directory: %s", os.getcwd())

# look for plugins in path:
#for path in sys.path: print("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx path=", path)

available_plugins = []
for module_info in pkgutil.iter_modules(path=None):
    module_name = module_info.name
    if module_name.startswith('_'):
        continue
    if module_name.startswith('ikb_plugin_'):
        available_plugins.append(module_name)
        _logger.debug("Found plugin:'%s', is_package=%s, path=%s",
                        module_info.name, 
                        module_info.ispkg,
                        module_info.module_finder.path)
        plugin_module = importlib.import_module(module_name)
        plugin_collection = Collection.from_module(plugin_module)
        ns.add_collection(plugin_collection, module_name)

    elif module_name=='ikb_plugins' and module_info.ispkg:
        for sub_module_info in pkgutil.iter_modules(
            path=['%s/%s' % (module_info.module_finder.path, module_name,)], 
            prefix='%s.'% module_name
        ):
            sub_module_name = sub_module_info.name
            available_plugins.append(sub_module_name)
            _logger.debug("Found plugin:'%s', is_package=%s, path=%s",
                          sub_module_info.name, 
                          sub_module_info.ispkg,
                          sub_module_info.module_finder.path)
            sub_plugin_module = importlib.import_module(sub_module_name)
            sub_plugin_collection = Collection.from_module(sub_plugin_module)
            ns.add_collection(sub_plugin_collection, sub_module_name[12:])

print("ikb 1.0 - inouk buildit - (c) 2020-2024 Cyril MORISSE")
print("   https://gitlab.com/inouk/buildit\n")

if available_plugins:
    print("Available ikb plugins:", ','.join(available_plugins))
else:
    print("No ikb plugins found!")

program = Program(
    namespace=ns, 
    version='0.1.0'
)
