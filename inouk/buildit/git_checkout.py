import importlib
import pathlib
import json
import logging
import os
import logging
import sys

from invoke import task, Collection
from invoke.config import DataProxy
from inouk.buildit.tasks import init as buildit_init

_logger = logging.getLogger(__name__)

plugin_module_dict = {}

BUILDIT_ROOT_DIRECTORY = None
RUNNING_ENV_ROOT_DIR = None

def git_checkout(c, part, repository_dict, target_directory=None):
    """ 
    """
    update = repository_dict['update']
    with c.cd(BUILDIT_ROOT_DIRECTORY):
        if not pathlib.Path(target_directory).exists():
            depth = repository_dict.get('depth')
            cmd_line = "git clone %(depth)s %(repository)s %(target_directory)s" % {
                "depth": "--depth=%s" % depth if depth else "",
                "repository": repository_dict['repository'],
                "target_directory": target_directory
            }
            result = c.run(cmd_line, warn=True, in_stream=False)
            update = True  # To checkout refspec @ initial clone

        if update:
            with c.cd(target_directory):
                c.run("git fetch", in_stream=False)
                refspec = repository_dict.get('refspec')
                if refspec:
                    c.run("git checkout %s" % refspec, in_stream=False)

                result = c.run("git pull", warn=True, in_stream=False)
                if result.failed:
                    if 'not currently on a branch' not in result.stderr and 'local changes to the following files would be overwritten by merge' not in result.stderr:
                        raise Exception("Error while pulling:'%s'." % repository_dict['repository'])

    return target_directory



@task(buildit_init)
def init(c, part):
    global BUILDIT_ROOT_DIRECTORY
    BUILDIT_ROOT_DIRECTORY = c.config.buildit['root_directory']

    global RUNNING_ENV_ROOT_DIR
    RUNNING_ENV_ROOT_DIR = c.config.buildit.get('running_env_root_dir', BUILDIT_ROOT_DIRECTORY)

    part = c.config.buildit.get(part)
    repositories_dict = part.get('repositories', {})
    for repo_key in repositories_dict:
        repository_dict = repositories_dict[repo_key]
        target_directory_norm = os.path.normpath(
            os.path.join(
                #BUILDIT_ROOT_DIRECTORY, 'buildit-plugins', repository_dict['directory']
                RUNNING_ENV_ROOT_DIR, '.ikb/plugins', repository_dict['directory']
            )
        )
        git_checkout(c, part, repository_dict, target_directory_norm)


@task(init)
def install(c, part):
    return

@task(init)
def reset(c, part):
    global BUILDIT_ROOT_DIRECTORY
    BUILDIT_ROOT_DIRECTORY = c.config.buildit['root_directory']
    global RUNNING_ENV_ROOT_DIR
    RUNNING_ENV_ROOT_DIR = c.config.buildit['running_env_root_dir']

    target_directory_norm = os.path.normpath(
        os.path.join(
            RUNNING_ENV_ROOT_DIR, '.ikb/plugins'
        )
    )
    _r = c.run(f"rm -rf {target_directory_norm}", warn=True)
    return
